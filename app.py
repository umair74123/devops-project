from flask import Flask, g, redirect, render_template, request, session, url_for



class User:

    def __init__(self, id, username, password):

        self.id = id

        self.username = username

        self.password = password



    def __repr__(self):

        return f'<User: {self.username}>'



users = []

users.append(User(id=1, username='ali', password='123'))

users.append(User(id=2, username='hamza', password='123'))

users.append(User(id=3, username='umair', password='123'))



app = Flask(__name__)

app.secret_key = 'somesecretkeythatonlyishouldknow'



@app.before_request

def before_request():

    g.user = None

    if 'user_id' in session:

        user = [user for user in users if user.id == session['user_id']]

        g.user = user[0] if user else None



@app.route('/', methods=['GET', 'POST'])

def login():

    if request.method == 'POST':

        session.pop('user_id', None)



        username = request.form['username']

        password = request.form['password']

        user = next((user for user in users if user.username == username), None)

        if user and user.password == password:

            session['user_id'] = user.id

            return redirect(url_for('profile'))

        else:

            message = 'Invalid username or password.'

            return render_template('login.html', message=message)



    return render_template('login.html')



@app.route('/profile')

def profile():

    if not g.user:

        return redirect(url_for('login'))



    return render_template('profile.html')



if __name__ == '__main__':

    app.run(debug=True)

